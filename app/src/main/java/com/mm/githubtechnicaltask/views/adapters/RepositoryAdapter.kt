package com.mm.githubtechnicaltask.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mm.githubtechnicaltask.R
import com.mm.githubtechnicaltask.model.Repo
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryAdapter(
    private val repositories: List<Repo>,
    private val clickListener: (Repo) -> Unit
) :
    RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = repositories.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository = repositories[position]
        holder.view.repo_name.text = repository.name

        holder.itemView.setOnClickListener {
            clickListener.invoke(repository)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
package com.mm.githubtechnicaltask.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mm.githubtechnicaltask.R
import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.utils.DataState
import com.mm.githubtechnicaltask.viewmodel.GitHubRepositoriesStateEvent
import com.mm.githubtechnicaltask.viewmodel.RepositoryViewModel
import com.mm.githubtechnicaltask.views.adapters.RepositoryAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_github_form.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class GitHubFormFragment : Fragment() {

    private lateinit var container: ConstraintLayout
    private var repositories = listOf<Repo>()

    private val viewModel: RepositoryViewModel by viewModels()
    private lateinit var adapter: RepositoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_github_form, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view as ConstraintLayout

        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar?.setDisplayHomeAsUpEnabled(false)

        subscribeObservers()

        viewModel.setStateEvent(
            GitHubRepositoriesStateEvent.GetReposHistoryStateEvent,
            null,
            null
        )

        search_button.setOnClickListener {
            val owner = owner_repo_field_input.text.toString().split("/")[0]
            val repoName = owner_repo_field_input.text.toString().split("/")[1]

            viewModel.setStateEvent(
                GitHubRepositoriesStateEvent.GetRepositoryStateEvent,
                owner,
                repoName
            )
        }

    }

    private fun subscribeObservers() {
        viewModel.repositoryDataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<Repo> -> {
                    showProgressBar(false)
                    goToCommits(dataState.data)
                }
                is DataState.Loading -> {
                    showProgressBar(true)
                }
                is DataState.Error -> {
                    showProgressBar(false)
                    Toast.makeText(
                        requireContext(),
                        dataState.exception.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
        viewModel.historyReposDataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<List<Repo>> -> {
                    showProgressBar(false)
                    repositories = dataState.data
                    fetchRepos()
                }
                is DataState.Loading -> {
                    showProgressBar(true)
                }
                is DataState.Error -> {
                    showProgressBar(false)
                    Toast.makeText(
                        requireContext(),
                        dataState.exception.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun showProgressBar(isDisplayed: Boolean) {
        progress_bar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun fetchRepos() {
        adapter = RepositoryAdapter(repositories) { repository ->
            goToCommits(repository)
        }
        history_repos.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = this@GitHubFormFragment.adapter
        }
        history_repos.addItemDecoration(
            DividerItemDecoration(history_repos.context, DividerItemDecoration.VERTICAL)
        )

        if (repositories.isEmpty()) {
            history_label.visibility = View.GONE
            history_repos.visibility = View.GONE
        } else {
            history_label.visibility = View.VISIBLE
            history_repos.visibility = View.VISIBLE
        }
    }

    private fun goToCommits(repo: Repo) {
        val bundle = bundleOf("repo" to repo)
        Navigation.findNavController(container)
            .navigate(R.id.action_gitHubFormFragment_to_commitsFragment, bundle)
    }
}
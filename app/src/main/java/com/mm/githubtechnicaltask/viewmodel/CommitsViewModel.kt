package com.mm.githubtechnicaltask.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mm.githubtechnicaltask.model.Commit
import com.mm.githubtechnicaltask.repository.CommitsRepository
import com.mm.githubtechnicaltask.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CommitsViewModel
@ViewModelInject
constructor(private val repository: CommitsRepository) : ViewModel() {

    private val _commitsDataState: MutableLiveData<DataState<List<Commit>>> = MutableLiveData()

    val commitsDataState: LiveData<DataState<List<Commit>>>
        get() = _commitsDataState

    fun setStateEvent(
        commitsStateEvent: CommitsStateEvent,
        owner: String?,
        repo: String?
    ) {
        viewModelScope.launch {
            when(commitsStateEvent) {
                is CommitsStateEvent.GetRemoteCommitsStateEvent -> {
                    owner?.let { owner ->
                        repo?.let { repo ->
                            repository.getCommits(owner, repo).onEach { dataState ->
                                _commitsDataState.value = dataState
                            }
                                .launchIn(viewModelScope)
                        }
                    }
                }
            }
        }
    }

}

sealed class CommitsStateEvent {
    object GetRemoteCommitsStateEvent : CommitsStateEvent()
}
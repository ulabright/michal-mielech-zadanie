package com.mm.githubtechnicaltask.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.repository.RepositoriesRepository
import com.mm.githubtechnicaltask.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class RepositoryViewModel
@ViewModelInject
constructor(private val repositoriesRepository: RepositoriesRepository) : ViewModel() {

    private val _repositoryDataState: MutableLiveData<DataState<Repo>> = MutableLiveData()
    private val _historyReposDataState: MutableLiveData<DataState<List<Repo>>> = MutableLiveData()

    val repositoryDataState: LiveData<DataState<Repo>>
        get() = _repositoryDataState

    val historyReposDataState: LiveData<DataState<List<Repo>>>
        get() = _historyReposDataState

    fun setStateEvent(
        gitHubRepositoriesStateEvent: GitHubRepositoriesStateEvent,
        owner: String?,
        repo: String?
    ) {
        viewModelScope.launch {
            when (gitHubRepositoriesStateEvent) {
                is GitHubRepositoriesStateEvent.GetRepositoryStateEvent -> {
                    owner?.let { owner ->
                        repo?.let { repo ->
                            repositoriesRepository.getGitHubRepository(owner, repo).onEach { dataState ->
                                _repositoryDataState.value = dataState
                            }
                                .launchIn(viewModelScope)
                        }
                    }
                }
                is GitHubRepositoriesStateEvent.GetReposHistoryStateEvent -> {
                    repositoriesRepository.getReposHistory().onEach { dataState ->
                        _historyReposDataState.value = dataState
                    }.launchIn(viewModelScope)
                }
            }
        }
    }
}

sealed class GitHubRepositoriesStateEvent {
    object GetRepositoryStateEvent : GitHubRepositoriesStateEvent()
    object GetReposHistoryStateEvent : GitHubRepositoriesStateEvent()
}
package com.mm.githubtechnicaltask.di

import com.mm.githubtechnicaltask.model.db.ProjectDAO
import com.mm.githubtechnicaltask.model.db.mappers.CommitCacheMapper
import com.mm.githubtechnicaltask.model.db.mappers.RepositoryCacheMapper
import com.mm.githubtechnicaltask.model.network.api.GitHubService
import com.mm.githubtechnicaltask.model.network.mappers.CommitNetworkMapper
import com.mm.githubtechnicaltask.model.network.mappers.RepositoryNetworkMapper
import com.mm.githubtechnicaltask.repository.CommitsRepository
import com.mm.githubtechnicaltask.repository.RepositoriesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRepositoriesRepository(
        projectDAO: ProjectDAO,
        gitHubService: GitHubService,
        repositoryCacheMapper: RepositoryCacheMapper,
        networkMapper: RepositoryNetworkMapper
    ): RepositoriesRepository {
        return RepositoriesRepository(projectDAO, gitHubService, repositoryCacheMapper, networkMapper)
    }

    @Singleton
    @Provides
    fun provideCommitsRepository(
        projectDAO: ProjectDAO,
        gitHubService: GitHubService,
        repositoryCacheMapper: CommitCacheMapper,
        networkMapper: CommitNetworkMapper
    ): CommitsRepository {
        return CommitsRepository(projectDAO, gitHubService, repositoryCacheMapper, networkMapper)
    }

}
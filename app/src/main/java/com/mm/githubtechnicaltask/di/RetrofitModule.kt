package com.mm.githubtechnicaltask.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mm.githubtechnicaltask.model.network.api.GitHubService
import com.mm.githubtechnicaltask.utils.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RetrofitModule {

    @Singleton
    @Provides
    fun provideGsonBuilder() = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson))

    @Singleton
    @Provides
    fun provideGitHubService(retrofit: Retrofit.Builder) =
        retrofit.build().create(GitHubService::class.java)

}
package com.mm.githubtechnicaltask.di

import android.content.Context
import androidx.room.Room
import com.mm.githubtechnicaltask.model.db.ProjectDAO
import com.mm.githubtechnicaltask.model.db.ProjectDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DbModule {

    @Singleton
    @Provides
    fun provideStationDb(@ApplicationContext context: Context): ProjectDatabase {
        return Room.databaseBuilder(
            context,
            ProjectDatabase::class.java,
            ProjectDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideStationDao(projectDatabase: ProjectDatabase): ProjectDAO {
        return projectDatabase.projectDao()
    }
}
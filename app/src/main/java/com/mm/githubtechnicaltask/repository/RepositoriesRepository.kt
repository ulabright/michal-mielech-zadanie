package com.mm.githubtechnicaltask.repository

import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.model.db.ProjectDAO
import com.mm.githubtechnicaltask.model.db.mappers.RepositoryCacheMapper
import com.mm.githubtechnicaltask.model.network.api.GitHubService
import com.mm.githubtechnicaltask.model.network.mappers.RepositoryNetworkMapper
import com.mm.githubtechnicaltask.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RepositoriesRepository
constructor(
    private val projectDAO: ProjectDAO,
    private val gitHubService: GitHubService,
    private val repositoryCacheMapper: RepositoryCacheMapper,
    private val networkMapper: RepositoryNetworkMapper
) {

    fun getGitHubRepository(owner: String, repo: String): Flow<DataState<Repo>> =
        flow {
            emit(DataState.Loading)
            try {
                val networkRepo = gitHubService.getRepo(owner, repo)
                val repository = networkMapper.mapFromEntity(networkRepo)
                projectDAO.insertRepository(repositoryCacheMapper.mapToEntity(repository))
                emit(DataState.Success(repository))
            } catch (e: Exception) {
                emit(DataState.Error(e))
            }
        }

    fun getReposHistory(): Flow<DataState<List<Repo>>> = flow {
        emit(DataState.Loading)
        try {
            val historyRepos = repositoryCacheMapper.mapFromEntityList(projectDAO.getCachedRepositories())
            emit(DataState.Success(historyRepos))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}
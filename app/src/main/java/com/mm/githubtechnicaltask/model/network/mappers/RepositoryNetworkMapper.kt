package com.mm.githubtechnicaltask.model.network.mappers

import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.model.network.networkentities.RepositoryNetworkEntity
import com.mm.githubtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class RepositoryNetworkMapper
@Inject
constructor() : EntityMapper<RepositoryNetworkEntity, Repo> {

    override fun mapFromEntity(entity: RepositoryNetworkEntity): Repo {
        return Repo(
            id = entity.id,
            name = entity.name
        )
    }

    override fun mapToEntity(domainModel: Repo): RepositoryNetworkEntity {
        return RepositoryNetworkEntity(
            id = domainModel.id,
            name = domainModel.name
        )
    }

}
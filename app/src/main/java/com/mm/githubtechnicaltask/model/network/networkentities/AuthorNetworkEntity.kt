package com.mm.githubtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AuthorNetworkEntity(
    @Expose
    @SerializedName("name")
    var name: String
)
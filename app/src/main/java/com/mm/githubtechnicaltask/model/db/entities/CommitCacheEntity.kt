package com.mm.githubtechnicaltask.model.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "commits")
data class CommitCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "sha")
    var sha: String,

    @ColumnInfo(name = "message")
    var message: String,

    @ColumnInfo(name = "authorName")
    var authorName: String,

    @ColumnInfo(name = "repoName")
    var repoName: String
)
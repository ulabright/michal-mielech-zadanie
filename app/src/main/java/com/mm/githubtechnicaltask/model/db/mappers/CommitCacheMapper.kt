package com.mm.githubtechnicaltask.model.db.mappers

import com.mm.githubtechnicaltask.model.Commit
import com.mm.githubtechnicaltask.model.db.entities.CommitCacheEntity
import com.mm.githubtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class CommitCacheMapper
@Inject
constructor() : EntityMapper<CommitCacheEntity, Commit> {
    override fun mapFromEntity(entity: CommitCacheEntity): Commit {
        return Commit(
            sha = entity.sha,
            authorName = entity.authorName,
            message = entity.message,
            repoName = entity.repoName
        )
    }

    override fun mapToEntity(domainModel: Commit): CommitCacheEntity {
        return CommitCacheEntity(
            sha = domainModel.sha,
            authorName = domainModel.authorName,
            message = domainModel.message,
            repoName = domainModel.repoName ?: ""
        )
    }

    fun mapFromEntityList(entities: List<CommitCacheEntity>): List<Commit> {
        return entities.map {
            mapFromEntity(it)
        }
    }

    fun mapToEntityList(commits: List<Commit>): List<CommitCacheEntity> {
        return commits.map {
            mapToEntity(it)
        }
    }

}
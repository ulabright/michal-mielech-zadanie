package com.mm.githubtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Repo(
    var id: String,

    var name: String

) : Parcelable
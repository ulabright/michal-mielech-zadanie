package com.mm.githubtechnicaltask.model

data class Commit(
    var sha: String,

    var message: String,

    var authorName: String,

    var repoName: String?
)
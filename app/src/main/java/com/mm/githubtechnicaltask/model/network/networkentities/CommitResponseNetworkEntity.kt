package com.mm.githubtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CommitResponseNetworkEntity(
    @Expose
    @SerializedName("sha")
    var sha: String,

    @Expose
    @SerializedName("commit")
    var commit: CommitNetworkEntity

)
# GitHub Technical Task

GitHub Technical Task is an application for reviewing commits in GitHub repositories.

## Installation

To install application you have to build apk file with Android Studio and then install it on device with Android 7.1 or higher.

## Usage

After running the app, user have view with field, to provide full repository name in format: <owner>/<repo>. After clicking on "Search" button, app will send request to GitHub with entered repository name and the user will be taken to list of commits in that repository.
Below repository name form is list of previously searched repositories.